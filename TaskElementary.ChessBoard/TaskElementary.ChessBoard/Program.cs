﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskElementary.ChessBoard
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter first number!");
            int firstNum;
            while (!int.TryParse(Console.ReadLine(), out firstNum))
            {
                Console.WriteLine("Not a number, try again!");
            }

            Console.WriteLine("Enter second number!");
            int secondNum;
            while (!int.TryParse(Console.ReadLine(), out secondNum))
            {
                Console.WriteLine("Not a number, try again!");
            }

            for (int i = 0; i < firstNum; ++i)
            {
                for (int j = 0; j < secondNum; ++j)
                {
                    if ((i + j) % 2 == 0)
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write("*");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
